<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

$api = app('Dingo\Api\Routing\Router');

 // Controllers namespaces
 $controllers =[
     'course' => 'App\Api\V1\Controllers\CourseController',
     'user' => 'App\Api\V1\Controllers\UserController',
     
    ];

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$api->version('v1', function ($api) use($controllers){
	
	//prefix is api/v1
	$api->group(['prefix' => 'v1','middleware' => 'cors'], function($api) use ($controllers){

		$api->get('courses', ['as' => 'get.course', 'uses' =>  $controllers['course'] .'@getCourse']);

		$api->post('course', ['as' => 'store.course', 'uses' =>  $controllers['course'] .'@storeCourse']);
		
		$api->put('course/{id}', ['as' => 'update.course', 'uses' =>  $controllers['course'] .'@updateCourse']);

		$api->delete('course/{id}', ['as' => 'delete.course', 'uses' =>  $controllers['course'] .'@deleteCourse']);

		$api->post('authenticate', ['as' => 'authenticate.user', 'uses' =>  $controllers['user'] .'@Authenticate']);
		
		$api->post('user', ['as' => 'store.user', 'uses' =>  $controllers['user'] .'@storeUser']);

		$api->group(['middleware' => 'api.auth'], function($api) use ($controllers){
			
			$api->get('user', ['as' => 'get.user', 'uses' =>  $controllers['user'] .'@getUser']);


	});
		

	});
	

	// $api->group(['middleware' => 'jwt.auth'], function($api) use ($controllers){

	// });
});

// //remember to create <div id ="image"></div>
// imgs= document.getElementById('image');
// for(var i= 0; i< data.length ; i++){
// 	//create div
// 	var element = document.createElement("div");
// 	//give the div id like img1
// 	element.setAttribute("id", "img"+i);
// 	//add the names to the created div
//     element.appendChild(document.createTextNode(data[i].name);
//     //append each image to the image div 
// 	imgs.appendChild(element);
// 	images.push(data[i].name)
// }
