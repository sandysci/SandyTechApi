<?php 

namespace App\Api\V1\Controllers;
use API;
use Hash;
use Image;
use JWTAuth;
use Validator;
use App\Api\V1\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Dingo\Api\Exception\StoreResourceFailedException;

class UserController extends Controller
{

 private $model;

 public function __construct(User $user){

 	$this->model = $user;

 } 

 public function storeUser(Request $request){
 	$data = $request->all();
 	$rules =[ 
 			'name' => 'min:3',
            'email' => 'email|unique:users,email|required',
            'password'=>'min:5|confirmed|required',
            ];
     $validator = Validator::make($data, $rules);
     if($validator->fails()){
     	return response()->json(['message' => 'could_not_create_User'.$validator->errors()], 500);
     	 // throw new StoreResourceFailedException('Could not create user. Errors: '. $validator->errors());
     }
    $data['password'] = Hash::make($data['password']);
 	$this->model->fill($data);
 	$this->model->save();
 	return $this->model;

 }

public function Authenticate(Request $request)
	{
		// $request->except('_token');

		$credentials = $request->only('email', 'password');

       try
        {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) 
            {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } 
        catch (JWTException $e)
        {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

       
        return response()->json(compact('token'));

	}

 public function getUser(){
 	$user = API::user();
 	return response()->json(compact('user'));
 }

public function getAuthenticatedUser()
{
    try {

        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }

    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

        return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

        return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

        return response()->json(['token_absent'], $e->getStatusCode());

    }
    // return $user->id;

    // the token is valid and we have found the user via the sub claim
    return response()->json(compact('user'));
}

}
