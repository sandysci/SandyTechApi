<?php 

namespace App\Api\V1\Controllers;
use Image;
use Illuminate\Http\Request;
use App\Api\V1\Models\Course;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
	private $model;

	public function __construct(Course $course) 
    {
      $this->model = $course;
       
    }
	public function getCourse()
	{
	 $courses = $this->model->orderBy('created_at','DESC')->get();
	
	 return response($courses, 200);
	}

	public function storeCourse(Request $request){
	
	 $data =$request->all();
	    if ($request->picture_url != null) 
	       {
	         	$img = Image::make($request->picture_url)->encode('jpg', 75);
	         	$filename = time().'.jpg';
	         	$filepath = 'files/courses/'.$filename;
	         	\Log::info($filepath);
	         	$filePath_merchant = $_SERVER['DOCUMENT_ROOT'].'/files/courses/'.$filename;
	         	\Log::info( $filePath_merchant);
	         	$img->save($filePath_merchant);
	         	$data['picture_url']=$filepath;
	       }
    // dd($this->model);
	$this->model->fill($data);
	$this->model->save();
	return response($this->model, 200);


	}

	public function deleteCourse($id){
	 $this->model = $this->model->find($id);
	 $this->model->delete();
	 return $this->getCourse();
	}

	public function updateCourse(Request $request,$id){
	 $this->model = $this->model->find($id);
	 $data = $request->all();
	  if ($request->picture_url != null) 
	       {
	         	$img = Image::make($request->picture_url)->encode('jpg', 75);
	         	$filename = time().'.jpg';
	         	$filepath = 'files/courses/'.$filename;
	         	\Log::info($filepath);
	         	$filePath_merchant = $_SERVER['DOCUMENT_ROOT'].'/files/courses/'.$filename;
	         	\Log::info( $filePath_merchant);
	         	$img->save($filePath_merchant);
	         	$data['picture_url']=$filepath;
	       }
	 $this->model->fill($data);
	 $this->model->save();
	 return $this->getCourse();
	}



}