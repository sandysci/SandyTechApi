<?php

namespace App\Api\V1\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    //
    protected $fillable = ['name', 'code', 'description','picture_url'];
}
