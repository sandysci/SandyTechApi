<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //
        DB::table('courses')->delete();
        DB::table('courses')->insert(array(
		
		array(
            'id' => '1',
			'name' => 'Java',
			'code' => 'Javac',
			'description' => 'Java is a general-purpose computer programming language that is concurrent, class-based, object-oriented, and specifically designed to have as few implementation dependencies as possible',
			'picture_url' => 'files/courses/java2.png',
            ),

		array(
			'id' => '2',
			'name' => 'Php',
			'code' => 'Php(laravel)',
			'description' => 'PHP (recursive acronym for PHP: Hypertext Preprocessor) is a widely-used open source general-purpose scripting language that is especially suited for web development and can be embedded into HTML.',
			'picture_url' => 'files/courses/php2.jpg',
			),

		array(
			'id' => '3',
			'name' => 'Python',
			'code' => 'Python(oop)',
			'description' => 'Python is a high-level programming language designed to be easy to read and simple to implement. It is open source, which means it is free to use, even for commercial applications. ... Python is considered a scripting language, like Ruby or Perl and is often used for creating Web applications and dynamic Web content',
			'picture_url' => 'files/courses/python2.png',
			),

		));

	
    }
}
